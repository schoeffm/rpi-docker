#!/bin/bash

if [ -z "$1" ]; then
    echo "No argument supplied"
    echo "Usage ./letsencrypt.sh <Your-Domain>"
    exit 1
fi

sudo docker run --rm -ti -v $(pwd)/dehydrated:/dehydrated/config \
	--volumes-from=rpinextcloud_nextcloud_1 schoeffm/rpi-dehydrated:v2 \
	-c -f /dehydrated/config/config --accept-terms -k /dehydrated/config/hook.sh -d $1
