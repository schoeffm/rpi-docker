Based on this image you can create a ready-to-be-used nextcloud-installation on your RaspberryPi (backed by a MySQL-Database). 

I did a lot of rework to the original images, to ensure support for letsencrypt-certificates as well as the self-signed default-certificates this setup will come with initially. So, you can consider this a version 2.

You can find the `Dockerfile`, all companion scripts as well as the `docker-compose.yml`-file in this [BitBucket-Repo][repo]

For a cut'n'paste able Screencast see the sections below.

## How's it all setup

In order to keep things separated and to support a smooth update-process for future nextcloud releases the whole installation consists of three different container.

	rpi-nextcloud  --+							// actual installation
	                 |
	                 +---> rpi-nextcloud-db		// mysql database-backend
	                 |
	                 +---> rpi-nextcloud-data	// volumes for uploaded files, configs 
                                                // and tls-certificates
	
This installation uses a mysql database as backend. The database is encapsulated in a dedicated container (`rpi-nextcloud-db`) which in turn is linked to the actual nextcloud-container (`rpi-nextcloud`). 

I also thought it would be benificial to separate the uploaded data, the configuration as well as the used tls-certificates from the actual nextcloud installation to make upgrades much easier. That's the reason for the `rpi-nextcloud-data` container which just provides volumes to store these data (_so it's not a running container but just acts as data-store - it will immediately stop after starting, don't be confused_).

## Usage / Installation

### Use docker-compose - (_recommended_)

To get the complete setup up and running with one command you should use `docker-compose` which pulls, creates and starts all necessary things. 

[![asciicast](https://asciinema.org/a/98127.png)](https://asciinema.org/a/98127)

So check out the repo, adjust the settings defined in `docker-compose.yml` to suit your needs and run `sudo docker-compose up -d` - that's it. 

*__Notice__: don't forget to change the passwords in the YAML-file*

### Or ... you could start everything by hand

So, how to get started. If you'd like to start everything manually you can do it this way:

[![asciicast](https://asciinema.org/a/98195.png)](https://asciinema.org/a/98195)

#### 1. create a dedicated network

To make sure that the nextcloud-container can access your mysql-container you should link 'em via a dedicated network

    sudo docker network create nextcloud-network

This makes communication between (and only between the attached container) possible.

#### 2. create a data-container

    sudo docker run -d --name data schoeffm/rpi-nextcloud-data

After the containers creation it will immediately stop running since its only purpose is to provide a bunch of volumes for our actual nextcloud-container - and for that it mustn't be running. As mentioned before, this should make updates of your nextcloud-container much easier since you don't have to worry about your uploaded files, your `config.php` or your tls-certificates. 

#### 3. start a MySQL-container

    sudo docker run -d -p 3306:3306 --name mysql \
        --net=nextcloud-network \
        -e NEXTCLOUD_DB_USER=nextcloud \
        -e NEXTCLOUD_DB_PASSWORD=mycloud \
        -e MYSQL_ROOT_PASSWORD=foo \
        schoeffm/rpi-nextcloud-mysql

This will start a mysql deamonized container. In order to be useful for our purposes we have to provide a password for the root-user as well as a username and password for the technical nextcloud user. The running container provides a `nextcloud`-schema which is accessible by the given user. 

#### 4. combine everything

    sudo docker run -d --name nextcloud -p 80:80 -p 443:443 \
        --net=nextcloud-network \
        --volumes-from data \
        -e NEXTCLOUD_DB_USER=nextcloud \
        -e NEXTCLOUD_DB_PASSWORD=mycloud \
        -e NEXTCLOUD_SERVERNAME=tmp.hopto.org \
        schoeffm/rpi-nextcloud

This starts the acutal nextcloud container which is accessible via port 80 and 443 (whereas 80 just redirects to 443 - we won't accept unsecured communication).

*__Notice__: don't forget to change the passwords to match those of your mysql-setup*
*__Notice__: be sure to provide a valid domain-name your installation will be accessible through*

## Configuration and means to adjust it

The overall setup is based on the very good tutorials of [Jan Karres][owncloud]. Some of the more important things include:

- [nginx][nginx_page] as web-server (smaller footprint, good performance)
- only secured communication over SSL (port 80 will be redirected to 443)
- thus, on first start a self-signed certificate will be created (be sure you provide a valid `NEXTCLOUD_SERVERNAME`)
- extended file-upload limits in your `php.ini` (2048M)
- preconfigured UTF-8 support
- ... _(see [here][nginx] and [here][owncloud] for more details)_

### Environment-Variables



The `Dockerfile` defines/uses the following environment variables to control the behaviour of our installation:

|variable   |used during   |default   |description   |
|---|---|---|---|
|NEXTCLOUD_VERION   |build   |_depends on version_   |Determines the nextcloud version to be used during image build   |
|NEXTCLOUD_CERT_DIR  |runtime   |/srv/http/ssl   |This is the location nginx will expect tls-certificates. The start-script will place it's self-signed certs in this location as well.   |
|NEXTCLOUD_DATA_DIR   |runtime   |/srv/http/nextcloud/data   |Specifies the directory where all uploaded data will be placed (also the log-files and other stuff are placed here). This directory should match one of the exported volumes of `rpi-nextcloud-data`.  |
|NEXTCLOUD_CONFIG_DIR   |runtime   |/var/www/nextcloud/config   |During installation the `autoconfig.php` is placed here - afterwards this is the location where nextcloud stores its `config.php`. Again, this directory should match a volume of `rpi-nextcloud-data`   |
|NEXTCLOUD_SERVERNAME   |runtime   |dockerpi   |This is the domain-name which is used during certificate creationas well as during nextcloud-setup (redirections etc.).<br/> __Notice:__ has to be changed!!   |
|NEXTCLOUD_DIFFIE_HELLMAN   |runtime   |on   |During first startup a PEM-file will be generated (along with the self-signed certificate) which is used to configure nginx to use [Diffie-Hellman][diffie] for key exchange. Since this generation can take a significant amount of time (depending on your RaspberryPi Version), you can deactivate it.   |
|NEXTCLOUD_DB_USER   |runtime   |nextcloud   |Well, should be clear what this is<br/>__Notice:__ has to be changed!!   |
|NEXTCLOUD_DB_PASSWORD   |runtime   |mycloud   |dto.   |

## Replace self-signed certs with letsencrypt certs

By default, when started for the first time, the `rpi-nextcloud`-container will generate a self-signed certificate (places it in a mounted volume and checks on every subsequent restart for its existance to ensure it's done only once). Because it's self-signed, browsers will consider this certificate as non-trustworthy.

Therefore, you should consider to provide a trustworthy certificate - like the ones [letsencrypt][letsencrypt] will issue. As luck would have it, we also have a proper [dehydrated][dehydrated]-image right at your disposal.

[![asciicast](https://asciinema.org/a/98130.png)](https://asciinema.org/a/98130)

Provided with a proper `config`-file you should be able to generate (and keep up to date) a set of valid certificates (_the `rpi-nextcloud`-repo already contains a suitable configuration as well as a trigger-shell script_). 
It should be easy to define a cron-job repeatedly updating these certs for you.

In combination with the nginx configuration contained in this image you should get a [level A rating][ssltest] for your setup.
![Rating](https://bitbucket.org/schoeffm/rpi-docker/raw/54b93e00ff46056f6e8bef06053bfb679ebe0ed6/rpi-nextcloud/doc/ssl_rating.png)

Also, as long as you keep the patch-level up-to-date a [level A rating][scantest] should also be possible for your nextcloud installation in general.
![Scan-Rating](https://bitbucket.org/schoeffm/rpi-docker/raw/54b93e00ff46056f6e8bef06053bfb679ebe0ed6/rpi-nextcloud/doc/scan_rating.png)

## How does an upgrade look like

Nextcloud provide different ways to upgrade an installation. Our approach is pretty much compareable with the [maunal upgrade][upgrade] - which you still should read through!

1. it is always wise to backup your data before you start to change your current setup
    - dump the database-content (i.e. by using the [mysql-workbench][mysql])
    - dump the data contained in our volume 

            sudo docker run --rm --volumes-from nextcloud-data \
                -v $(pwd):/backup resin/rpi-raspbian \
                tar cvf /backup/nextcloud-data-backup.tar /srv

2. now, turn on maintenance mode
    
        sudo docker exec -u www-data -d nextcloud \
            php /var/www/nextcloud/occ maintenance:mode --on

2. stop the currently running instance: 
        
        sudo docker stop nextcloud
3. start the new version by creating a new container (_as we've described above - don't forget to give it a new name or remove the old one first_)
4. now, execute the upgrade script (_within the new, running container_)

        sudo docker exec -u www-data -d nextcloud \
            php /var/www/nextcloud/occ upgrade

5. finally, turn off the maintenance mode again
    
        sudo docker exec -u www-data -d nextcloud \
            php /var/www/nextcloud/occ maintenance:mode --off

That's it!

[repo]:https://bitbucket.org/schoeffm/rpi-docker/src/acb764e933b38ff486f62417717cdf9d1a18d7fc/rpi-nextcloud/?at=master
[docker-compose]:http://blog.hypriot.com/post/docker-compose-nodejs-haproxy/
[nginx]:http://jankarres.de/2012/08/raspberry-pi-webserver-nginx-installieren/
[owncloud]:http://jankarres.de/2013/10/raspberry-pi-owncloud-server-installieren/
[nginx_page]:http://nginx.com
[upgrade]:https://docs.nextcloud.org/server/9/admin_manual/maintenance/upgrade.html?highlight=upgrade
[mysql]:https://www.mysql.de/products/workbench/
[letsencrypt]:https://letsencrypt.org/
[dehydrated]:https://github.com/lukas2511/dehydrated
[diffie]:https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange
[ssltest]:https://www.ssllabs.com/ssltest/
[scantest]:https://scan.nextcloud.com/

