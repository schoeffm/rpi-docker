<?php
$AUTOCONFIG = array(
    "directory" => "$OWNCLOUD_DATA_DIR",
    "dbtype" => "mysql",
    "dbname" => "owncloud",
    "dbuser" => "$OWNCLOUD_DB_USER",
    "dbpass" => "$OWNCLOUD_DB_PASSWORD",
    "dbhost" => "mysql",
    "dbtableprefix" => "oc_",
);
