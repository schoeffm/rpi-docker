Based on this image you can create a ready-to-be-used owncloud-installation on your RaspberryPi (backed by a MySQL-Database). 

I did a lot of rework to the original images, to ensure support for letsencrypt-certificates as well as the self-signed default-certificates this setup will come with initially. So, you can consider this a version 2.

You can find the `Dockerfile`, all companion scripts as well as the `docker-compose.yml`-file in this [BitBucket-Repo][repo]

For a cut'n'paste able Screencast see the sections below.

## How's it all setup

In order to keep things separated and to support a smooth update-process for future owncloud releases the whole installation consists of three different container.

	rpi-owncloud  --+							// actual installation
	                 |
	                 +---> rpi-owncloud-db		// mysql database-backend
	                 |
	                 +---> rpi-owncloud-data	// volumes for uploaded files, configs 
                                                // and tls-certificates
	
This installation uses a mysql database as backend. The database is encapsulated in a dedicated container (`rpi-owncloud-db`) which in turn is linked to the actual owncloud-container (`rpi-owncloud`). 

I also thought it would be benificial to separate the uploaded data, the configuration as well as the used tls-certificates from the actual owncloud installation to make upgrades much easier. That's the reason for the `rpi-owncloud-data` container which just provides volumes to store these data (_so it's not a running container but just acts as data-store - it will immediately stop after starting, don't be confused_).

## Usage / Installation

### Use docker-compose - (_recommended_)

To get the complete setup up and running with one command you should use `docker-compose` which pulls, creates and starts all necessary things. 

[![asciicast](https://asciinema.org/a/98127.png)](https://asciinema.org/a/98127)
*__NOTICE__: Don't be confuesed! The screencast above demonstrates the setup using a `nextcloud`-image - not the owncloud-image which is described here! Since the setup-process is exactly the same I spared recording a redundant screencast.*

So check out the repo, adjust the settings defined in `docker-compose.yml` to suit your needs and run `sudo docker-compose up -d` - that's it. 

*__Notice__: don't forget to change the passwords in the YAML-file*

### Or ... you could start everything by hand

So, how to get started. If you'd like to start everything manually you can do it this way:

[![asciicast](https://asciinema.org/a/98195.png)](https://asciinema.org/a/98195)
*__NOTICE__: Don't be confuesed! The screencast above demonstrates the setup using a `nextcloud`-image - not the owncloud-image which is described here! Since the setup-process is exactly the same I spared recording a redundant screencast.*

#### 1. create a dedicated network

To make sure that the owncloud-container can access your mysql-container you should link 'em via a dedicated network

    sudo docker network create owncloud-network

This makes communication between (and only between the attached container) possible.

#### 2. create a data-container

    sudo docker run -d --name data schoeffm/rpi-owncloud-data

After the containers creation it will immediately stop running since its only purpose is to provide a bunch of volumes for our actual owncloud-container - and for that it mustn't be running. As mentioned before, this should make updates of your owncloud-container much easier since you don't have to worry about your uploaded files, your `config.php` or your tls-certificates. 

#### 3. start a MySQL-container

    sudo docker run -d -p 3306:3306 --name mysql \
        --net=owncloud-network \
        -e OWNCLOUD_DB_USER=owncloud \
        -e OWNCLOUD_DB_PASSWORD=mycloud \
        -e MYSQL_ROOT_PASSWORD=foo \
        schoeffm/rpi-owncloud-mysql

This will start a mysql deamonized container. In order to be useful for our purposes we have to provide a password for the root-user as well as a username and password for the technical owncloud user. The running container provides a `owncloud`-schema which is accessible by the given user. 

#### 4. combine everything

    sudo docker run -d --name owncloud -p 80:80 -p 443:443 \
        --net=owncloud-network \
        --volumes-from data \
        -e OWNCLOUD_DB_USER=owncloud \
        -e OWNCLOUD_DB_PASSWORD=mycloud \
        -e OWNCLOUD_SERVERNAME=tmp.hopto.org \
        schoeffm/rpi-owncloud

This starts the acutal owncloud container which is accessible via port 80 and 443 (whereas 80 just redirects to 443 - we won't accept unsecured communication).

*__Notice__: don't forget to change the passwords to match those of your mysql-setup*
*__Notice__: be sure to provide a valid domain-name your installation will be accessible through*

## Configuration and means to adjust it

The overall setup is based on the very good tutorials of [Jan Karres][owncloud]. Some of the more important things include:

- [nginx][nginx_page] as web-server (smaller footprint, good performance)
- only secured communication over SSL (port 80 will be redirected to 443)
- thus, on first start a self-signed certificate will be created (be sure you provide a valid `OWNCLOUD_SERVERNAME`)
- extended file-upload limits in your `php.ini` (2048M)
- preconfigured UTF-8 support
- ... _(see [here][nginx] and [here][owncloud] for more details)_

### Environment-Variables



The `Dockerfile` defines/uses the following environment variables to control the behaviour of our installation:

|variable   |used during   |default   |description   |
|---|---|---|---|
|OWNCLOUD_VERION   |build   |_depends on version_   |Determines the owncloud version to be used during image build   |
|OWNCLOUD_CERT_DIR  |runtime   |/srv/http/ssl   |This is the location nginx will expect tls-certificates. The start-script will place it's self-signed certs in this location as well.   |
|OWNCLOUD_DATA_DIR   |runtime   |/srv/http/owncloud/data   |Specifies the directory where all uploaded data will be placed (also the log-files and other stuff are placed here). This directory should match one of the exported volumes of `rpi-owncloud-data`.  |
|OWNCLOUD_CONFIG_DIR   |runtime   |/var/www/owncloud/config   |During installation the `autoconfig.php` is placed here - afterwards this is the location where owncloud stores its `config.php`. Again, this directory should match a volume of `rpi-owncloud-data`   |
|OWNCLOUD_SERVERNAME   |runtime   |dockerpi   |This is the domain-name which is used during certificate creationas well as during owncloud-setup (redirections etc.).<br/> __Notice:__ has to be changed!!   |
|OWNCLOUD_DIFFIE_HELLMAN   |runtime   |on   |During first startup a PEM-file will be generated (along with the self-signed certificate) which is used to configure nginx to use [Diffie-Hellman][diffie] for key exchange. Since this generation can take a significant amount of time (depending on your RaspberryPi Version), you can deactivate it.   |
|OWNCLOUD_DB_USER   |runtime   |owncloud   |Well, should be clear what this is<br/>__Notice:__ has to be changed!!   |
|OWNCLOUD_DB_PASSWORD   |runtime   |mycloud   |dto.   |

## Replace self-signed certs with letsencrypt certs

By default, when started for the first time, the `rpi-owncloud`-container will generate a self-signed certificate (places it in a mounted volume and checks on every subsequent restart for its existance to ensure it's done only once). Because it's self-signed, browsers will consider this certificate as non-trustworthy.

Therefore, you should consider to provide a trustworthy certificate - like the ones [letsencrypt][letsencrypt] will issue. As luck would have it, we also have a proper [dehydrated][dehydrated]-image right at your disposal.

[![asciicast](https://asciinema.org/a/98130.png)](https://asciinema.org/a/98130)
*__NOTICE__: Don't be confuesed! The screencast above demonstrates the setup using a `nextcloud`-image - not the owncloud-image which is described here! Since the setup-process is exactly the same I spared recording a redundant screencast.*

Provided with a proper `config`-file you should be able to generate (and keep up to date) a set of valid certificates (_the `rpi-owncloud`-repo already contains a suitable configuration as well as a trigger-shell script_). 
It should be easy to define a cron-job repeatedly updating these certs for you.

In combination with the nginx configuration contained in this image you should get a [level A rating][ssltest] for your setup.
![Rating](https://bytebucket.org/schoeffm/rpi-docker/raw/eca503f48654d476272fd380c48fab16a2fb1cb7/rpi-nextcloud/doc/ssl_rating.png)

## How does an upgrade look like

owncloud provide different ways to upgrade an installation. Our approach is pretty much compareable with the [maunal upgrade][upgrade] - which you still should read through!

1. it is always wise to backup your data before you start to change your current setup
    - dump the database-content (i.e. by using the [mysql-workbench][mysql])
    - dump the data contained in our volume 

            sudo docker run --rm --volumes-from owncloud-data \
                -v $(pwd):/backup resin/rpi-raspbian \
                tar cvf /backup/owncloud-data-backup.tar /srv

2. now, turn on maintenance mode
    
        sudo docker exec -u www-data -d owncloud \
            php /var/www/owncloud/occ maintenance:mode --on

2. stop the currently running instance: 
        
        sudo docker stop owncloud
3. start the new version by creating a new container (_as we've described above - don't forget to give it a new name or remove the old one first_)
4. now, execute the upgrade script (_within the new, running container_)

        sudo docker exec -u www-data -d owncloud \
            php /var/www/owncloud/occ upgrade

5. finally, turn off the maintenance mode again
    
        sudo docker exec -u www-data -d owncloud \
            php /var/www/owncloud/occ maintenance:mode --off

That's it!

[repo]:https://bitbucket.org/schoeffm/rpi-docker/src/4322959c0411d84d5d6711068d969e373e872920/rpi-owncloud/?at=master
[docker-compose]:http://blog.hypriot.com/post/docker-compose-nodejs-haproxy/
[nginx]:http://jankarres.de/2012/08/raspberry-pi-webserver-nginx-installieren/
[owncloud]:http://jankarres.de/2013/10/raspberry-pi-owncloud-server-installieren/
[nginx_page]:http://nginx.com
[upgrade]:https://doc.owncloud.org/server/9.1/admin_manual/maintenance/upgrade.html
[mysql]:https://www.mysql.de/products/workbench/
[letsencrypt]:https://letsencrypt.org/
[dehydrated]:https://github.com/lukas2511/dehydrated
[diffie]:https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange
[ssltest]:https://www.ssllabs.com/ssltest/
